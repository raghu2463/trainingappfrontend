import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
  public registerModel = {
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    phoneNumber: "",
    headLine: "",
    userImage: "",
    linkedIn: "",
    github: "",
    youTube: "",
    roleId: "",
    status: "",
  };
  constructor() {}

  ngOnInit() {}

  register() {}
}
