import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { UsersRouting } from './courses.routing';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-modal';
import { CoursesComponent } from './courses.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        NgxEchartsModule,
        UsersRouting,
        NgxPaginationModule,
        FormsModule,
        ModalModule,
    ],
    declarations: [
        CoursesComponent,
    ]
})
export class CoursesModule { }
