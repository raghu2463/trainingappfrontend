import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { TablesDataService } from "../table/components/data-table/tablesData.service";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.scss"],
  providers: [TablesDataService],
})
export class UsersComponent implements OnInit {
  tableData: Array<any>;
  filteredTableData: Array<any>;
  contactIdList: Array<any> = [];
  contactId: any;
  contactName: any;
  pageSize = 10;
  pageNumber = 1;
  searchText: string;
  contactIntreactionTypeList: Array<any> = [];
  limitText = [];

  constructor(
    private router: Router,
    private tablesDataService: TablesDataService
  ) {}

  ngOnInit() {
    this.tableData = [
      {
        id: 1,
        firstName: "Mark",
        lastName: "Otto",
        username: "@mdo",
        email: "mdo@gmail.com",
        age: "28",
      },
      {
        id: 2,
        firstName: "Jacob",
        lastName: "Thornton",
        username: "@fat",
        email: "fat@yandex.ru",
        age: "45",
      },
      {
        id: 3,
        firstName: "Larry",
        lastName: "Bird",
        username: "@twitter",
        email: "twitter@outlook.com",
        age: "18",
      },
      {
        id: 4,
        firstName: "John",
        lastName: "Snow",
        username: "@snow",
        email: "snow@gmail.com",
        age: "20",
      },
      {
        id: 5,
        firstName: "Jack",
        lastName: "Sparrow",
        username: "@jack",
        email: "jack@yandex.ru",
        age: "30",
      },
      {
        id: 6,
        firstName: "Ann",
        lastName: "Smith",
        username: "@ann",
        email: "ann@gmail.com",
        age: "21",
      },
      {
        id: 7,
        firstName: "Barbara",
        lastName: "Black",
        username: "@barbara",
        email: "barbara@yandex.ru",
        age: "43",
      },
      {
        id: 8,
        firstName: "Sevan",
        lastName: "Bagrat",
        username: "@sevan",
        email: "sevan@outlook.com",
        age: "13",
      },
      {
        id: 9,
        firstName: "Ruben",
        lastName: "Vardan",
        username: "@ruben",
        email: "ruben@gmail.com",
        age: "22",
      },
      {
        id: 10,
        firstName: "Karen",
        lastName: "Sevan",
        username: "@karen",
        email: "karen@yandex.ru",
        age: "33",
      },
      {
        id: 11,
        firstName: "Mark",
        lastName: "Otto",
        username: "@mark",
        email: "mark@gmail.com",
        age: "38",
      },
      {
        id: 12,
        firstName: "Jacob",
        lastName: "Thornton",
        username: "@jacob",
        email: "jacob@yandex.ru",
        age: "48",
      },
      {
        id: 13,
        firstName: "Haik",
        lastName: "Hakob",
        username: "@haik",
        email: "haik@outlook.com",
        age: "48",
      },
      {
        id: 14,
        firstName: "Garegin",
        lastName: "Jirair",
        username: "@garegin",
        email: "garegin@gmail.com",
        age: "40",
      },
      {
        id: 15,
        firstName: "Krikor",
        lastName: "Bedros",
        username: "@krikor",
        email: "krikor@yandex.ru",
        age: "32",
      },
    ];
    this.filteredTableData = this.tableData;
  }

  loadData(){

  }

  // Data table page cheange method
  pageChanged(pN: number): void {
    this.pageNumber = pN;
    this.pageSize = 10;
    console.log(this.pageNumber);
  }

  searchOption(event) {
    const value = this.searchText;
    this.filteredTableData = Object.assign([], this.tableData).filter(
      item => {
        return item.firstName.toLowerCase().indexOf(value.toLowerCase()) > -1;
      });
  }

}
