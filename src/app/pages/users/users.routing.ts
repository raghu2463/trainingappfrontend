import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';

const childRoutes: Routes = [
    {
        path: '',
        component: UsersComponent
    }
];

export const UsersRouting = RouterModule.forChild(childRoutes);
