import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { ForgotPasswordComponent } from './pages/forgotPassword/forgotPassword.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },

  {
    path: 'register',
    component: RegisterComponent
  },

  {
    path: 'forgotPassword',
    component: ForgotPasswordComponent
  },

];

export const routing = RouterModule.forRoot(appRoutes);
